package br.com.sysplacas.sysplacas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysplacasApplication {

	public static void main(String[] args) {
		SpringApplication.run(SysplacasApplication.class, args);
	}

}
